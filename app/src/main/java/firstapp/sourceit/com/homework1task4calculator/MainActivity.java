package firstapp.sourceit.com.homework1task4calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText number1;
    EditText number2;

    Button btnAdd;
    Button btnSub;
    Button btnMult;
    Button btnDiv;

    TextView Result;

    String operation = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        number1 = (EditText) findViewById(R.id.etNum1);
        number2 = (EditText) findViewById(R.id.etNum2);

        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnSub = (Button) findViewById(R.id.btnSub);
        btnMult = (Button) findViewById(R.id.btnMult);
        btnDiv = (Button) findViewById(R.id.btnDiv);

        Result = (TextView) findViewById(R.id.Result);

        // set a listener
        btnAdd.setOnClickListener(this);
        btnSub.setOnClickListener(this);
        btnMult.setOnClickListener(this);
        btnDiv.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
            int num1 = 0;
            int num2 = 0;
            int result = 0;

            // check if the fields are empty
            if (TextUtils.isEmpty(number1.getText().toString())
                    || TextUtils.isEmpty(number2.getText().toString())) {
                return;
            }

            // read EditText and fill variables with numbers
            num1 = Integer.parseInt(number1.getText().toString());
            num2 = Integer.parseInt(number2.getText().toString());

            switch (v.getId()) {
                case R.id.btnAdd:
                    operation = "+";
                    result = num1 + num2;
                    break;
                case R.id.btnSub:
                    operation = "-";
                    result = num1 - num2;
                    break;
                case R.id.btnMult:
                    operation = "*";
                    result = num1 * num2;
                    break;
                case R.id.btnDiv:
                    operation = "/";
                    result = num1 / num2;
                    break;
                default:
                    break;
            }

            // form the output line
        Result.setText(num1 + " " + operation + " " + num2 + " = " + result);
        }

}